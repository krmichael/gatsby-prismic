import React from "react"
import { Link, graphql } from "gatsby"
import { RichText } from "prismic-reactjs"

import { linkResolver } from "../utils/linkResolver"
import Layout from "../components/layout"
import SEO from "../components/seo"

function BlogPosts({ posts = [] }) {
  return (
    <div>
      {posts.map(post => (
        <div key={post.node._meta.id}>
          <Link to={linkResolver(post.node._meta)}>
            {RichText.asText(post.node.title)}
          </Link>

          <p>
            <time>{post.node.date}</time>
          </p>
        </div>
      ))}
    </div>
  )
}

export default ({ data }) => {
  const doc = data.prismic.allBlog_homes.edges.slice(0, 1).pop()
  const posts = data.prismic.allPosts.edges

  if (!doc) return null

  return (
    <Layout>
      <SEO title="Home" />
      <div>
        <img src={doc.node.image.url} alt={doc.node.image.alt} />
        <h1>{RichText.asText(doc.node.headline)}</h1>
        <p>{RichText.asText(doc.node.description)}</p>
      </div>

      <BlogPosts posts={posts} />
      <Link to="/page-2/">Go to page 2</Link>
    </Layout>
  )
}

export const query = graphql`
  query {
    prismic {
      allBlog_homes {
        edges {
          node {
            headline
            description
            image
          }
        }
      }
      allPosts(sortBy: date_DESC) {
        edges {
          node {
            _meta {
              id
              uid
              type
            }
            title
            date
          }
        }
      }
    }
  }
`
