import React from "react"
import { graphql, Link } from "gatsby"
import { RichText } from "prismic-reactjs"

export default ({ data }) => {
  const doc = data.prismic.allPosts.edges.slice(0, 1).pop()

  if (!doc) return null

  return (
    <div>
      <Link to="/">
        <span>go back</span>
      </Link>

      <h1>{RichText.asText(doc.node.title)}</h1>

      <span>
        <em>{doc.node.date}</em>
      </span>

      <div>{RichText.render(doc.node.post_body)}</div>
    </div>
  )
}

export const query = graphql`
  query BlogPostQuery($uid: String) {
    prismic {
      allPosts(uid: $uid) {
        edges {
          node {
            title
            date
            post_body
          }
        }
      }
    }
  }
`
